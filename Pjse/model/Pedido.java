package model;

public class Pedido {

	Produto prod = new Produto();
	private String nome;
	private String end;
	private String desc;
	
	public Pedido(String nome, String end, String desc) {
		this.nome = nome;
		this.end = end;
		this.desc = desc;
	}
	
	public Pedido() {
		
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
