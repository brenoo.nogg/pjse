package model;

public class Produto {
    private String nome;
    private double qnt;
    private double valor; // valor do produto

	public Produto(String nome, double valor, double qnt){
		this.nome = nome; 
		this.valor = valor;
		this.qnt = qnt;
	}
	public Produto(){

	}
	public double getValor(){
		return valor;
	}
	public void setValor(double valor){
		this.valor = valor;
	}
	public String getNome(){
		return nome;
	}
	public void setNome(String nome){
		this.nome = nome;
	}

    public double getQnt() {
        return qnt;
    }

    public void setQnt(double qnt) {
        this.qnt = qnt;
    }

}
