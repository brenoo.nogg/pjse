import java.util.Scanner;
import model.Produto;
import model.Pedido;
public class Main {
	public static Scanner leia = new Scanner(System.in);
    public static void main(String args[]){
    	Produto prod = new Produto();
    	System.out.println("[-----------------------------------------------------]");
    	System.out.println("Entrar com o nome do Produto: ");
        System.out.print("Nome: ");
        prod.setNome(leia.next());
        System.out.print("Preço: ");
        prod.setValor(leia.nextDouble()); 
        System.out.print("Quantidade: ");
        prod.setQnt(leia.nextInt());
        System.out.println("[-----------------------------------------------------]");
        System.out.println("Deseja fazer um pedido ? [S] SIM | [N] NÃO");
        String resposta = leia.next();
        
   
        
        if(resposta.equalsIgnoreCase("S")) {
        	Pedido ped = new Pedido();
        	System.out.print("Nome do Cliente: "); 
        	ped.setNome(leia.next());
        	System.out.print("Endereço: ");
        	ped.setEnd(leia.next());
        	System.out.print("Descrição: ");
        	ped.setDesc(leia.next());
        }
        
        leia.close();
        
        
    }
	
  
}
